from crossword_entry import CrosswordEntry

def is_across(crossword: list[str], row: int, col: int) -> bool:

    '''Checks whether a wordspace in a crossword puzzle is across.

    Args:
        crossword: The crossword puzzle to be checked
        row: The row at which the wordspace begins
        col: The column at which the wordspace begins

    Returns:
        Whether the wordspace is across or not
    '''

    if col == 0:

        # No need to check left as it is the left-most column
        return crossword[row][col+1] != '#'

    elif 0 < col < len(crossword[row]):

        return crossword[row][col-1] == '#' and crossword[row][col+1] != '#'

    # Not possible to be across if a * is at the last column of a row
    return False


def is_down(crossword: list[str], row: int, col: int) -> bool:

    '''Checks whether a wordspace in a crossword puzzle is down.

    Args:
        crossword: The crossword puzzle to be checked
        row: The row at which the wordspace begins
        col: The column at which the wordspace begins

    Returns:
        Whether the wordspace is down or not
    '''

    if row == 0:

        # No need to check above if we are in the first row
        return crossword[row+1][col] != '#'

    elif 0 < row < len(crossword):

        return crossword[row-1][col] == '#' and crossword[row+1][col] != '#'

    # # It is not possible to be down if a * is in the last row
    return False


def get_across_length(crossword: list[str], row: int, col: int) -> int:

    '''Obtains the length of an across wordspace so we can determine which words are viable for it.

    Args:
        crossword: The crossword puzzle to be checked
        row: The row at which the wordspace begins
        col: The column at which the wordspace begins

    Returns:
        The length of the across wordspace
    '''

    across_length = 0

    while col < len(crossword[row]):

        if crossword[row][col] != "#":

            across_length += 1

        else:

            break

        col += 1

    return across_length


def get_down_length(crossword: list[str], row: int, col: int) -> int:

    '''Obtains the length of a down wordspace so we can determine which words are viable for it.

    Args:
        crossword: The crossword puzzle to be checked
        row: The row at which the wordspace begins
        col: The column at which the wordspace begins

    Returns:
        The length of the down wordspace
    '''

    down_length = 0

    while row < len(crossword):

        if crossword[row][col] != "#":

            down_length += 1

        else:

            break

        row += 1

    return down_length


def check_completion(crossword: list[str]) -> bool:

    '''Checks if the crossword is complete by looking for empty spaces.

    Args:
        crossword: The crossword puzzle to be checked

    Returns:
        Whether there are any empty spaces
    '''

    spaces_remaining = 0

    for row in crossword:

        for char in row:

            if char == '_':

                spaces_remaining += 1

    return spaces_remaining == 0


def print_crossword(crossword: list[str]) -> None:

    '''Prints a given crossword puzzle.

    Args:
        crossword: The crossword puzzle to be printed
    '''

    print()

    for row in crossword:

        print(row)

    print()


def remove_last_word(crossword: list[str], entry: CrosswordEntry) -> list[str]:

    '''Removes the last word placed on the crossword puzzle. Takes into account overlapping words

    Args:
        crossword: The crossword puzzle to have its last word removed
        entry: The current wordspace we are modifying

    Returns:
        The crossword puzzle with its last placed word removed
    '''

    # Get where the last word started and its length
    row, col = entry.start_point
    word_length = len(entry.possible_words[0])

    if entry.direction == 'Across':

        new_row = []

        for i in range(col, col + word_length):

            # If we are in the first row and there is a letter below, leave the letter
            if row == 0 and crossword[row+1][i].isalpha():

                new_row.append(crossword[row][i])

            # If we are in a "middle" row and there is a letter above or below, leave it
            elif (0 < row < len(crossword) - 1) and (crossword[row-1][i].isalpha() or
                                                     crossword[row+1][i].isalpha()):

                new_row.append(crossword[row][i])

            # If we are in the last row and there is a letter above, leave the letter
            elif row == len(crossword) - 1 and crossword[row-1][i].isalpha():

                new_row.append(crossword[row][i])

            else:

                new_row.append('_')

        new_row = ''.join(new_row)
        crossword[row] = ''.join([crossword[row][:col], new_row, crossword[row][col+word_length:]])

    if entry.direction == 'Down':

        for i in range(row, row + word_length):

            # If we are in the first column and there is a letter left, leave the letter
            if col == 0 and crossword[i][col+1].isalpha():

                continue

            # If we are in the "middle" and there is a letter left or right, leave this space alone
            elif (0 < col < len(crossword[row]) - 1) and (crossword[i][col-1].isalpha() or
                                                          crossword[i][col+1].isalpha()):

                continue

            # If we are in the last column and there is a letter right, leave the letter
            elif col == len(crossword[row]) - 1 and crossword[i][col-1].isalpha():

                continue

            # Not executed unless all statements above are false
            crossword[i] = ''.join([crossword[i][:col], '_', crossword[i][col+1:]])

    return crossword
